﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Foods : Waters
    {
        public string Food { get; set; }
        public int Count { get; set; }
        public int Price { get; set; }
        public Dictionary<string, int> FoodPrices = new Dictionary<string, int>()
        {
            { "Хлеб", 10 },
            { "Булочка", 15 },
            { "Чипсы", 34 },
            { "Печенье", 29 },
        };
    }
}