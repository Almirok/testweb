﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Models
{
    public class Ingredients
    {
        public bool Milk { get; set; }
        public bool Sugar { get; set; }
        public bool Syrup { get; set; }
        public bool Chees { get; set; }
        public bool Ham { get; set; }
        public bool Jam { get; set; }
        public bool Complex { get; set; }


        public List<int> Price = new List<int>() { 10, 5, 3, 15, 10, 7 };
    }
}