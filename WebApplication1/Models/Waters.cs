﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Waters : Ingredients
    {
        public string Water { get; set; }
        [Range(1,5)]

        public Dictionary<string, int> WaterPrices = new Dictionary<string, int>()
        {
            { "Вода", 20 },
            { "Эспрессо", 50 },
            { "Латте", 60 },
            { "Капучино", 70 },
            { "Черный чай", 25 },
            { "Зеленый чай", 25 }
        };
    }
}