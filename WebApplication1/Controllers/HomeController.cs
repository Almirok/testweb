﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        int sum = 0;
        string recept;
        public Dictionary<string, int> Ingredient = new Dictionary<string, int>();

        public ActionResult Index(Foods model)
        {
            model.Count = 1;
            return View(model);
        }

        public void Check(Foods model)
        {
            if (model.Milk)
            {
                Ingredient.Add("Молоко", 10);
            }
            if (model.Sugar)
            {
                Ingredient.Add("Сахар", 3);
            }
            if (model.Syrup)
            {
                Ingredient.Add("Сироп", 5);
            }
            if (model.Ham)
            {
                Ingredient.Add("Ветчина", 15);
            }
            if (model.Chees)
            {
                Ingredient.Add("Сыр", 10);
            }
            if (model.Jam)
            {
                Ingredient.Add("Джем", 7);
            }
        }

        [HttpPost]
        public ActionResult Index(string Food, string Water, Foods model)
        {
            Check(model);
            ViewBag.Message = null;
            foreach (var x in model.WaterPrices)
            {

                if (Water == x.Key)
                {
                    sum += x.Value;
                    if (Water == "Латте")
                    {
                        recept += x.Key + " (Эспрессо + Молоко); ";
                    }
                    if (Water == "Капучино")
                    {
                        recept += x.Key + " (Эспрессо + Молоко + Вода); ";
                    }
                }
            }
            foreach (var x in model.FoodPrices)
            {
                if (Food == x.Key)
                {
                    sum += x.Value;
                    if (Food == "Хлеб")
                    {
                        if (model.Ham && model.Chees)
                        {
                            recept += "Бутерброд (Хлеб + Ветчина + Сыр); ";
                            break;
                        }
                        if (model.Ham)
                        {
                            recept += "Бутерброд (Хлеб + Ветчина); ";
                        }
                        if (model.Chees)
                        {
                            recept += "Бутерброд (Хлеб + Сыр); ";
                        }

                    }
                    if (Food == "Булочка")
                    {
                        if (model.Ham && model.Chees)
                        {
                            recept += "Бутерброд (Булочка + Ветчина + Сыр); ";
                            break;
                        }
                        if (model.Ham)
                        {
                            recept += "Бутерброд (Булочка + Ветчина); ";
                        }
                        if (model.Chees)
                        {
                            recept += "Бутерброд (Булочка + Сыр); ";
                        }

                    }
                    else recept += x.Key + "; ";

                }
            }
            foreach (var x in Ingredient)
            {
                if (x.Key == "Сахар")
                {
                    sum += x.Value * model.Count;
                    recept += x.Key + " (" + model.Count + "); ";
                }
                else sum += x.Value;
                if (x.Key == "Ветчина" && (Food == "Хлеб" || Food == "Булочка"))
                {

                }
                else if (x.Key == "Сыр" && (Food == "Хлеб" || Food == "Булочка"))
                {

                }
                else recept += x.Key + " ;";

            }
            if (model.Complex)
            {
                sum = 90;
            }
            if (ModelState.IsValid)
            {
                TempData["testmsg"] = "<script>alert('" + recept.Remove(recept.Length - 2) + "; Цена:" + sum.ToString() + " ');</script>";
            }
            model.Price = sum;
            model.Count = 1;
            Ingredient.Clear();
            return RedirectToAction("Index", model);
        }

    }
}